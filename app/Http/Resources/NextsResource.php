<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NextsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tournament = [
            'id' => $this->id,
            'team_a'=> $this->team_a,
            'team_b'=> $this->team_b
        ];
        //return parent::toArray($request);
        return $tournament;
    }
}
