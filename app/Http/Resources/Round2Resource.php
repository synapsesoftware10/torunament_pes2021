<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Round2Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $round2 = [
            'id' => $this->id,
            'team_a'=> $this->team_a,
            'team_b'=> $this->team_b
        ];
        return $round2;
    }
}
