<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TournamentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $tournament = [
            'id' => $this->id,
            'descrizione'=> $this->descrizione
        ];
        //return parent::toArray($request);
        return $tournament;
    }
}
