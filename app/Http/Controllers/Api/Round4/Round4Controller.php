<?php

namespace App\Http\Controllers\Api\Round4;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Rounds4\Contracts\Round4ServiceInterface;



class Round4Controller extends Controller
{
    //
    public function index( $id,Round4ServiceInterface $round4Service)
    {
        // Recupero tutti i tornei dal service
        $round4 = $round4Service->getAll($id);

        return response()->json($round4);
    }

    public function ramodx( $id,Round4ServiceInterface $round4Service)
    {
        // Recupero tutti i tornei dal service
        $round4 = $round4Service->getAlldx($id);

        return response()->json($round4);
    }
}
