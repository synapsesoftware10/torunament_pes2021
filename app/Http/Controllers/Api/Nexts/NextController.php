<?php

namespace App\Http\Controllers\Api\Nexts;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Nexts\Contracts\NextsServiceInterface;

class NextController extends Controller
{
    public function index( Request $request, NextsServiceInterface $nextService)
    {

        $nexts = $nextService->getAll();
        return response()->json($nexts);
    }
}
