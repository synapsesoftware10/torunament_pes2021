<?php

namespace App\Http\Controllers\Api\Round32;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Rounds32\Contracts\Round32ServiceInterface;



class Round32Controller extends Controller
{
    //
    public function index( $id,Round32ServiceInterface $round32Service)
    {
        // Recupero tutti i tornei dal service
        $round32 = $round32Service->getAll($id);

        return response()->json($round32);
    }

    public function ramodx( $id,Round32ServiceInterface $round32Service)
    {
        // Recupero tutti i tornei dal service
        $round32 = $round32Service->getAlldx($id);

        return response()->json($round32);
    }
}
