<?php

namespace App\Http\Controllers\Api\Tournaments;

use App\Http\Controllers\Controller;
use App\Http\Resources\TournamentResource;
use App\Models\Tournament;
use App\Services\Tournaments\Contracts\TournamentServiceInterface;
use Illuminate\Http\Request;



class TournamentController extends Controller
{
    //
    public function index( Request $request, TournamentServiceInterface $tournamentService)
    {
        // Recupero tutti i tornei dal service
        //$alltournament = $tournamentService->getAll();
        $response = $tournamentService->getAll();
        return response()->json($response);
    }

    //
    public function show( $id,TournamentServiceInterface $tournamentService)
    {
        // Recupero tutti i tornei dal service
        $tournament = $tournamentService->getDetail($id);

        return response()->json($tournament);
    }

    public function tornei(){
        $alltornei = Tournament::all();
        return response()->json($alltornei);
    }
}
