<?php

namespace App\Http\Controllers\Api\Tables;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Tables\Contracts\TableServiceInterface;
use App\Services\Rounds32\Contracts\Round32ServiceInterface;



class TableController extends Controller
{
    //
    public function index( $id,TableServiceInterface $tableService)
    {
        // Recupero tutti i tornei dal service
        $tables = $tableService->getAll($id);

        return response()->json($tables);
    }
}
