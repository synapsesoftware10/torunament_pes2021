<?php

namespace App\Http\Controllers\Api\Matchs;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Matchs\Contracts\MatchsServiceInterface;

class MatchController extends Controller
{
    public function index( Request $request, MatchsServiceInterface $matchService)
    {

        $matchs = $matchService->getAll();
        return response()->json($matchs);
    }
}
