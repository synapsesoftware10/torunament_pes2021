<?php

namespace App\Http\Controllers\Api\Round2;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Rounds2\Contracts\Round2ServiceInterface;



class Round2Controller extends Controller
{
    //
    public function index( $id,Round2ServiceInterface $round2Service)
    {
        // Recupero tutti i tornei dal service
        $round2 = $round2Service->getAll($id);

        return response()->json($round2);
    }
}
