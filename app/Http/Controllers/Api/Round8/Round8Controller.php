<?php

namespace App\Http\Controllers\Api\Round8;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Rounds8\Contracts\Round8ServiceInterface;



class Round8Controller extends Controller
{
    //
    public function index( $id,Round8ServiceInterface $round8Service)
    {
        // Recupero tutti i tornei dal service
        $round8 = $round8Service->getAll($id);

        return response()->json($round8);
    }
    public function ramodx( $id,Round8ServiceInterface $round8Service)
    {
        // Recupero tutti i tornei dal service
        $round8 = $round8Service->getAlldx($id);

        return response()->json($round8);
    }
}
