<?php

namespace App\Http\Controllers\Api\Round16;

use App\Models\Round16;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Rounds16\Contracts\Round16ServiceInterface;



class Round16Controller extends Controller
{
    //
    public function index( $id,Round16ServiceInterface $round16Service)
    {
        // Recupero tutti i tornei dal service
        $round16 = $round16Service->getAll($id);

        return response()->json($round16);
    }

    public function ramodx( $id,Round16ServiceInterface $round16Service)
    {
        // Recupero tutti i tornei dal service
        $round16 = $round16Service->getAlldx($id);

        return response()->json($round16);
    }

    public function all( $id,Round16ServiceInterface $round16Service)
    {
        // Recupero tutti i tornei dal service
        $round16 = $round16Service->total($id);

        return response()->json($round16);
    }

    public function update(Request $request, Round16 $round16)
    {

            $data = $round16->update($request->all());

        //return response()->json(["data"=>$data],200);
    }
}
