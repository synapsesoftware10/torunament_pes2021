<?php

namespace App\Services\Tables;

use App\Services\Core\BaseService;
use App\Http\Resources\TableResource;
use App\Services\Tables\Contracts\TableServiceInterface;
use App\Repositories\Rounds16\Contracts\Round16RepositoryInterface;
use App\Repositories\Rounds32\Contracts\Round32RepositoryInterface;





class TableService extends BaseService implements TableServiceInterface

{

    private $round32Repository;
    private $round16Repository;

    public function __construct(
        Round32RepositoryInterface  $round32Repository,
        Round16RepositoryInterface  $round16Repository
        )
    {
       $this->round32Repository =  $round32Repository;
       $this->round16Repository =  $round16Repository;
    }

    public function getAll(int $id)
    {
        //
        $rounds32 = $this->round32Repository->all($id);
        $rounds16 = $this->round16Repository->all($id);

        //$this->response = TableResource::collection($rounds32,$rounds16);

        $this->response = [
            'round32' => $rounds32,

        ];

        return $this->parseResponse();

    }


}
