<?php

namespace App\Services\Rounds2;

use App\Services\Core\BaseService;
use App\Http\Resources\Round2Resource;
use App\Services\Rounds2\Contracts\Round2ServiceInterface;
use App\Repositories\Rounds2\Contracts\Round2RepositoryInterface;






class Round2Service extends BaseService implements Round2ServiceInterface

{

    private $round2Repository;

    public function __construct(Round2RepositoryInterface  $rounds2Repository)
    {
       $this->round2Repository =  $rounds2Repository;
    }

    public function getAll(int $id)
    {
       //
       $round2 = $this->round2Repository->all($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round2Resource::collection($round2);

       return $this->parseResponse();
    }


}
