<?php

namespace App\Services\Rounds8\Contracts;

interface Round8ServiceInterface{

    // implementazione delle funzioni

    public function getAll(int $id);
    public function getAlldx(int $id);

}
