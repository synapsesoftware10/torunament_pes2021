<?php

namespace App\Services\Rounds8;

use App\Services\Core\BaseService;
use App\Http\Resources\Round8Resource;
use App\Services\Rounds8\Contracts\Round8ServiceInterface;
use App\Repositories\Rounds8\Contracts\Round8RepositoryInterface;






class Round8Service extends BaseService implements Round8ServiceInterface

{

    private $round8Repository;

    public function __construct(Round8RepositoryInterface  $rounds8Repository)
    {
       $this->round8Repository =  $rounds8Repository;
    }

    public function getAll(int $id)
    {
       //
       $round8 = $this->round8Repository->all($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round8Resource::collection($round8);

       return $this->parseResponse();
    }

    public function getAlldx(int $id)
    {
       //
       $round8 = $this->round8Repository->alldx($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round8Resource::collection($round8);

       return $this->parseResponse();
    }


}
