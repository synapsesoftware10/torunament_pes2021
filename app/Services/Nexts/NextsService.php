<?php

namespace App\Services\Nexts;

use App\Services\Core\BaseService;
use App\Http\Resources\NextsResource;
use App\Http\Resources\MatchsResource;
use App\Services\Nexts\Contracts\NextsServiceInterface;
use App\Repositories\Nexts\Contracts\NextRepositoryInterface;





class NextsService extends BaseService implements NextsServiceInterface

{

    private $nextRepository;

    public function __construct(NextRepositoryInterface  $nextRepository)
    {
       $this->nextRepository =  $nextRepository;
    }

    public function getAll()
    {
       //
       $nexts = $this->nextRepository->all();

       //$this->response = ['torneo' => $response];

       $this->response = NextsResource::collection($nexts);

       return $this->parseResponse();
    }


}
