<?php

namespace App\Services\Rounds32;

use App\Services\Core\BaseService;
use App\Http\Resources\Round32Resource;
use App\Services\Rounds32\Contracts\Round32ServiceInterface;
use App\Repositories\Rounds32\Contracts\Round32RepositoryInterface;





class Round32Service extends BaseService implements Round32ServiceInterface

{

    private $round32Repository;

    public function __construct(Round32RepositoryInterface  $rounds32Repository)
    {
       $this->round32Repository =  $rounds32Repository;
    }

    public function getAll(int $id)
    {
       //
       $round32 = $this->round32Repository->all($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round32Resource::collection($round32);

       return $this->parseResponse();
    }

    public function getAlldx(int $id)
    {
       //
       $round32 = $this->round32Repository->alldx($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round32Resource::collection($round32);

       return $this->parseResponse();
    }


}
