<?php

namespace App\Services\Rounds32\Contracts;

interface Round32ServiceInterface{

    // implementazione delle funzioni

    public function getAll(int $id);
    public function getAlldx(int $id);

}
