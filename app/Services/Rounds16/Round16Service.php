<?php

namespace App\Services\Rounds16;

use App\Services\Core\BaseService;
use App\Http\Resources\Round16Resource;
use App\Services\Rounds16\Contracts\Round16ServiceInterface;
use App\Repositories\Rounds16\Contracts\Round16RepositoryInterface;






class Round16Service extends BaseService implements Round16ServiceInterface

{

    private $round16Repository;

    public function __construct(Round16RepositoryInterface  $rounds16Repository)
    {
       $this->round16Repository =  $rounds16Repository;
    }

    public function getAll(int $id)
    {
       //
       $round16 = $this->round16Repository->all($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round16Resource::collection($round16);

       return $this->parseResponse();
    }

    public function getAlldx(int $id)
    {
       //
       $round16 = $this->round16Repository->alldx($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round16Resource::collection($round16);

       return $this->parseResponse();
    }

    public function total(int $id)
    {
       //
       $round16 = $this->round16Repository->total($id);

       $this->response = Round16Resource::collection($round16);

       return $this->parseResponse();
    }

}
