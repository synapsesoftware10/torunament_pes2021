<?php

namespace App\Services\Rounds16\Contracts;

interface Round16ServiceInterface{

    // implementazione delle funzioni

    public function getAll(int $id);
    public function getAlldx(int $id);
    public function total(int $id);

}
