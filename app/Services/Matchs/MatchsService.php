<?php

namespace App\Services\Matchs;

use App\Services\Core\BaseService;
use App\Http\Resources\MatchsResource;
use App\Services\Matchs\Contracts\MatchsServiceInterface;
use App\Repositories\Matchs\Contracts\MatchRepositoryInterface;





class MatchsService extends BaseService implements MatchsServiceInterface

{

    private $matchRepository;

    public function __construct(MatchRepositoryInterface  $matchRepository)
    {
       $this->matchRepository =  $matchRepository;
    }

    public function getAll()
    {
       //
       $matchs = $this->matchRepository->all();

       //$this->response = ['torneo' => $response];

       $this->response = MatchsResource::collection($matchs);

       return $this->parseResponse();
    }


}
