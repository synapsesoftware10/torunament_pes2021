<?php

namespace App\Services\Tournaments;

use App\Http\Resources\TournamentResource;
use App\Services\Core\BaseService;
use App\Services\Tournaments\Contracts\TournamentServiceInterface;
use App\Repositories\Tournaments\Contracts\TournamentRepositoryInterface;


class TournamentService extends BaseService implements TournamentServiceInterface
{
    private $tournamentRepository;

    public function __construct(TournamentRepositoryInterface  $tournamentRepository)
    {
       $this->tournamentRepository =  $tournamentRepository;
    }

    public function getAll()
    {
        //
        $tounaments = $this->tournamentRepository->all();

        //$this->response = ['torneo' => $response];

        $this->response = TournamentResource::collection($tounaments);

        return $this->parseResponse();

    }

    public function getDetail(int $id)
    {
        //
        $tournament = $this->tournamentRepository->findById($id);

        $this->response = $tournament;

        return $this->parseResponse();

    }
}
