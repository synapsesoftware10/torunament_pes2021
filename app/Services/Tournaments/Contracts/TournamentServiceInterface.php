<?php

namespace App\Services\Tournaments\Contracts;

interface TournamentServiceInterface{

    // implementazione delle funzioni

    public function getDetail(int $id);
    public function getAll();


}
