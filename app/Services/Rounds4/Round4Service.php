<?php

namespace App\Services\Rounds4;

use App\Services\Core\BaseService;
use App\Http\Resources\Round4Resource;
use App\Services\Rounds4\Contracts\Round4ServiceInterface;
use App\Repositories\Rounds4\Contracts\Round4RepositoryInterface;






class Round4Service extends BaseService implements Round4ServiceInterface

{

    private $round4Repository;

    public function __construct(Round4RepositoryInterface  $rounds4Repository)
    {
       $this->round4Repository =  $rounds4Repository;
    }

    public function getAll(int $id)
    {
       //
       $round4 = $this->round4Repository->all($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round4Resource::collection($round4);

       return $this->parseResponse();
    }

    public function getAlldx(int $id)
    {
       //
       $round4 = $this->round4Repository->alldx($id);

       //$this->response = ['torneo' => $response];

       $this->response = Round4Resource::collection($round4);

       return $this->parseResponse();
    }


}
