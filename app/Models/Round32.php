<?php

namespace App\Models;

use App\Models\Tournament;
use Illuminate\Database\Eloquent\Model;

class Round32 extends Model
{
    // Black List -->campi inseriti tranne id perchè autogenerato dal database
    protected $guarded = ['id'];

    public function tornament()
    {
        return $this->belongsTo(Tournament::class);
    }
}
