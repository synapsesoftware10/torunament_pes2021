<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Round16 extends Model
{

    // Black List -->campi inseriti tranne id perchè autogenerato dal database
    protected $guarded = ['id'];

    public function torunament()
    {
        return $this->belongsTo(Tournament::class);
    }
}
