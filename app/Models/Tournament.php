<?php

namespace App\Models;

use App\Models\Round2;
use App\Models\Round4;
use App\Models\Round8;
use App\Models\Round16;
use App\Models\Round32;
use Illuminate\Database\Eloquent\Model;

class Tournament extends Model
{


    protected $fillable = [
        'competition',
        'descrizione',
    ];

    public function rel_round32()
    {
        return $this->hasMany(Round32::class);
    }

    public function rel_round16()
    {
        return $this->hasMany(Round16::class);
    }

    public function rel_round8()
    {
        return $this->hasMany(Round8::class);
    }

    public function rel_round4()
    {
        return $this->hasMany(Round4::class);
    }

    public function rel_round2()
    {
        return $this->hasMany(Round2::class);
    }


}
