<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class round8 extends Model
{
    // Black List -->campi inseriti tranne id perchè autogenerato dal database
    protected $guarded = ['id'];

    public function torunament()
    {
        return $this->belongsTo(Tournament::class);
    }
}
