<?php

namespace App\Providers;




//services
 // tournaments


 // rounds32
 use App\Services\Rounds32\Round32Service;
 use App\Services\Rounds32\Contracts\Round32ServiceInterface;
//repositories

use App\Services\Tables\TableService;
use App\Services\Rounds2\Round2Service;
 // rounds32




use App\Services\Rounds4\Round4Service;
use App\Services\Rounds8\Round8Service;

use Illuminate\Support\ServiceProvider;
use App\Services\Rounds16\Round16Service;
use App\Repositories\Rounds2\Round2Repository;
use App\Repositories\Rounds4\Round4Repository;
use App\Repositories\Rounds8\Round8Repository;
use App\Services\Tournaments\TournamentService;
use App\Repositories\Rounds16\Round16Repository;
use App\Repositories\Rounds32\Round32Repository;
use App\Repositories\Tournaments\TournamentRepository;
use App\Services\Tables\Contracts\TableServiceInterface;
use App\Services\Rounds2\Contracts\Round2ServiceInterface;
use App\Services\Rounds4\Contracts\Round4ServiceInterface;
use App\Services\Rounds8\Contracts\Round8ServiceInterface;
use App\Services\Rounds16\Contracts\Round16ServiceInterface;
use App\Repositories\Nexts\Contracts\NextRepositoryInterface;
use App\Repositories\Matchs\Contracts\MatchRepositoryInterface;
use App\Repositories\Rounds2\Contracts\Round2RepositoryInterface;
use App\Repositories\Rounds4\Contracts\Round4RepositoryInterface;
use App\Repositories\Rounds8\Contracts\Round8RepositoryInterface;
use App\Services\Tournaments\Contracts\TournamentServiceInterface;
use App\Repositories\Rounds16\Contracts\Round16RepositoryInterface;
use App\Repositories\Rounds32\Contracts\Round32RepositoryInterface;
use App\Repositories\Tournaments\Contracts\TournamentRepositoryInterface;

 // matchs
 use App\Services\Matchs\MatchsService;
 use App\Repositories\Matchs\MatchRepository;
 use App\Services\Matchs\Contracts\MatchsServiceInterface;

 // nexts
 use App\Services\Nexts\NextsService;
 use App\Repositories\Nexts\NextRepository;
 use App\Services\Nexts\Contracts\NextsServiceInterface;




class ContractServiceProvider extends ServiceProvider
{

    public $bindings = [

        // Services
        TournamentServiceInterface::class => TournamentService::class,
        Round32ServiceInterface::class => Round32Service::class,
        Round16ServiceInterface::class => Round16Service::class,
        Round8ServiceInterface::class => Round8Service::class,
        Round4ServiceInterface::class => Round4Service::class,
        Round2ServiceInterface::class => Round2Service::class,
        TableServiceInterface::class => TableService::class,
        MatchsServiceInterface::class => MatchsService::class,
        NextsServiceInterface::class => NextsService::class,

        // Repositories
        TournamentRepositoryInterface::class => TournamentRepository::class,
        Round32RepositoryInterface::class => Round32Repository::class,
        Round16RepositoryInterface::class => Round16Repository::class,
        Round8RepositoryInterface::class => Round8Repository::class,
        Round4RepositoryInterface::class => Round4Repository::class,
        Round2RepositoryInterface::class => Round2Repository::class,
        MatchRepositoryInterface::class => MatchRepository::class,
        NextRepositoryInterface::class => NextRepository::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
