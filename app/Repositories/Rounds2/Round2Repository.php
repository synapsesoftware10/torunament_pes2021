<?php
namespace App\Repositories\Rounds2;


use App\Models\Round2;
use Illuminate\Support\Collection;
use App\Repositories\Rounds2\Contracts\Round2RepositoryInterface;


class Round2Repository implements Round2RepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Round2();
    }



    public function all(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id);

        //return $this->getModel()->where('fk_tournament',$id);
    }
}
