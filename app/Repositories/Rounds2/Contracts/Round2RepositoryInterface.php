<?php

namespace App\Repositories\Rounds2\Contracts;

use Illuminate\Support\Collection;

interface Round2RepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function all(int $id): Collection;

}
