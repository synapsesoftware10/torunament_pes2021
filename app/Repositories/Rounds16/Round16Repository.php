<?php
namespace App\Repositories\Rounds16;


use App\Models\Round16;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Repositories\Rounds16\Contracts\Round16RepositoryInterface;


class Round16Repository implements Round16RepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Round16();
    }



    public function all(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','sx');

        //return $this->getModel()->where('fk_tournament',$id);
    }

    public function alldx(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','dx');

        //return $this->getModel()->where('fk_tournament',$id);
    }

    public function total(int $id): Collection
    {
        return $this->getModel()->all()->where('fk_tournament',$id);

    }




}
