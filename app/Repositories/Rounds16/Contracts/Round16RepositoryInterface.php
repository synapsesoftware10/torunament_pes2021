<?php

namespace App\Repositories\Rounds16\Contracts;

use Illuminate\Support\Collection;


interface Round16RepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function all(int $id): Collection;
    public function alldx(int $id): Collection;
    public function total(int $id): Collection;

}
