<?php

namespace App\Repositories\Rounds8\Contracts;

use Illuminate\Support\Collection;

interface Round8RepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function all(int $id): Collection;
    public function alldx(int $id): Collection;

}
