<?php
namespace App\Repositories\Rounds8;


use App\Models\Round8;
use Illuminate\Support\Collection;
use App\Repositories\Rounds8\Contracts\Round8RepositoryInterface;


class Round8Repository implements Round8RepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Round8();
    }



    public function all(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','sx');

        //return $this->getModel()->where('fk_tournament',$id);
    }

    public function alldx(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','dx');

    }
}
