<?php
namespace App\Repositories\Tournaments;

use App\Models\Tournament;
//use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Tournaments\Contracts\TournamentRepositoryInterface;
use Illuminate\Support\Collection;


class TournamentRepository implements TournamentRepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Tournament();
    }

    // Cerco per id
    public function findById(int $id)
    {
        return $this->getModel()->where('id',$id)->first();
    }

    public function all() : Collection
    {
        return  $this->getModel()->all();
    }
}
