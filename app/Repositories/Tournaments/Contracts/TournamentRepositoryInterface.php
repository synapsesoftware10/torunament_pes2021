<?php

namespace App\Repositories\Tournaments\Contracts;

use Illuminate\Support\Collection;
//use Illuminate\Database\Eloquent\Collection;

interface TournamentRepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function findById(int $id);

    public function all() : Collection;

}
