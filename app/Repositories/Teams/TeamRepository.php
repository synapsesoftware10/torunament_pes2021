<?php
namespace App\Repositories\Teams;

use App\Models\Teams;
//use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Teams\Contracts\TeamRepositoryInterface;
use Illuminate\Support\Collection;


class TournamentRepository implements TeamRepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Teams();
    }



    public function all() : Collection
    {
        return  $this->getModel()->all();
    }
}
