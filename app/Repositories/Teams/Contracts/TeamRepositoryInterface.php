<?php

namespace App\Repositories\Teams\Contracts;

use Illuminate\Support\Collection;
//use Illuminate\Database\Eloquent\Collection;

interface TeamRepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    //public function findById(int $id);

    public function all() : Collection;

}
