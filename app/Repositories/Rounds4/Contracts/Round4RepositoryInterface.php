<?php

namespace App\Repositories\Rounds4\Contracts;

use Illuminate\Support\Collection;

interface Round4RepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function all(int $id): Collection;
    public function alldx(int $id): Collection;

}
