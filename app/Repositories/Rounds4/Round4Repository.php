<?php
namespace App\Repositories\Rounds4;


use App\Models\Round4;
use Illuminate\Support\Collection;
use App\Repositories\Rounds4\Contracts\Round4RepositoryInterface;


class Round4Repository implements Round4RepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Round4();
    }



    public function all(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('posizione2','alto');

        //return $this->getModel()->where('fk_tournament',$id);
    }
    public function alldx(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('posizione2','basso');

    }
}
