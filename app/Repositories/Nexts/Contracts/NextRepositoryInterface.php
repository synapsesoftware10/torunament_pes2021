<?php

namespace App\Repositories\Nexts\Contracts;

use Illuminate\Support\Collection;
//use Illuminate\Database\Eloquent\Collection;

interface NextRepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    //public function findById(int $id);

    public function all();

}
