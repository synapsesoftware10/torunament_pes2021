<?php
namespace App\Repositories\Nexts;


use App\Models\Nexts;
//use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Nexts\Contracts\NextRepositoryInterface;
use Illuminate\Support\Collection;


class NextRepository implements NextRepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Nexts();
    }



    public function all()
    {
        return  $this->getModel()->limit(3)->orderBy('created_at')->get();
    }
}
