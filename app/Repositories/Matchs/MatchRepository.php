<?php
namespace App\Repositories\Matchs;

use App\Models\Matchs;
//use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Matchs\Contracts\MatchRepositoryInterface;
use Illuminate\Support\Collection;


class MatchRepository implements MatchRepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Matchs();
    }



    public function all()
    {
        return  $this->getModel()->limit(3)->orderBy('created_at')->get();
    }
}
