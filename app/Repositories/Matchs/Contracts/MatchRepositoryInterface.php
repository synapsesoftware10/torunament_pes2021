<?php

namespace App\Repositories\Matchs\Contracts;

use Illuminate\Support\Collection;
//use Illuminate\Database\Eloquent\Collection;

interface MatchRepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    //public function findById(int $id);

    public function all();

}
