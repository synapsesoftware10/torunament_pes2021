<?php
namespace App\Repositories\Rounds32;


use App\Models\Round32;
use Illuminate\Support\Collection;
use App\Repositories\Rounds32\Contracts\Round32RepositoryInterface;


class Round32Repository implements Round32RepositoryInterface{
    // Importo im modello dei dati
    public function getModel()
    {
        return new Round32();
    }



    public function all(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','sx');

        //return $this->getModel()->where('fk_tournament',$id);
    }

    public function alldx(int $id): Collection
    {

        return $this->getModel()->all()->where('fk_tournament',$id)->where('fascia','dx');

        //return $this->getModel()->where('fk_tournament',$id);
    }
}
