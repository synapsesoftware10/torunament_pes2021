<?php

namespace App\Repositories\Rounds32\Contracts;

use Illuminate\Support\Collection;

interface Round32RepositoryInterface{

    // implementazione delle funzioni

    public function getModel();

    public function all(int $id): Collection;
    public function alldx(int $id): Collection;

}
