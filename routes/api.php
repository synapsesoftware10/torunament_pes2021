<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Nexts\NextController;
use App\Http\Controllers\Api\Matchs\MatchController;
use App\Http\Controllers\Api\Tables\TableController;
use App\Http\Controllers\Api\Round2\Round2Controller;
use App\Http\Controllers\Api\Round4\Round4Controller;
use App\Http\Controllers\Api\Round8\Round8Controller;
use App\Http\Controllers\Api\Round16\Round16Controller;
use App\Http\Controllers\Api\Round32\Round32Controller;
use App\Http\Controllers\Api\Tournaments\TournamentController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Tournament
    // List Tournament
    Route::get('/tournaments', [TournamentController::class,'index']);
Route::get('/tornei', [TournamentController::class,'tornei']);

     // Dettaglio Tournament
     Route::get('/tournaments/{id}', [TournamentController::class,'show']);


     // List round32
     Route::get('/rounds32/{id}', [Round32Controller::class,'index']);
     Route::get('/rounds32dx/{id}', [Round32Controller::class,'ramodx']);
     // List round16
     Route::get('/rounds16/{id}', [Round16Controller::class,'index']);
     Route::get('/rounds16dx/{id}', [Round16Controller::class,'ramodx']);
     Route::get('/total16/{id}', [Round16Controller::class,'all']);
     Route::put('/rounds16/{round16}{scorea}', [Round16Controller::class,'update']);
      // List round8
      Route::get('/rounds8/{id}', [Round8Controller::class,'index']);
      Route::get('/rounds8dx/{id}', [Round8Controller::class,'ramodx']);
      // List round4
      Route::get('/rounds4/{id}', [Round4Controller::class,'index']);
      Route::get('/rounds4dx/{id}', [Round4Controller::class,'ramodx']);
      // List round2
      Route::get('/rounds2/{id}', [Round2Controller::class,'index']);

// Recent
Route::get('/recent', [MatchController::class,'index']);

// Next
Route::get('/next', [NextController::class,'index']);
