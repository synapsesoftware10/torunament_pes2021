<?php

namespace Database\Factories;

use App\Models\Matchs;
use Illuminate\Database\Eloquent\Factories\Factory;

class MatchsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Matchs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
            return [
                'team_a' => $this->faker->name,
                'team_b' => $this->faker->name,
                'score_a' => rand(0,4),
                'score_b' => rand(0,4),
                'logo_a' => $this->faker->image,
                'logo_b' => $this->faker->image,
                'category' => 'recent',
            ];

    }
}
